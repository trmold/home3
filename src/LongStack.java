import java.util.LinkedList;
import java.util.Iterator;

//use reference material
//https://enos.itcollege.ee/~jpoial/algoritmid/adt.html
//https://enos.itcollege.ee/~ylari/I231/Intstack.java
public class LongStack {

   //creating long type list for LIFO
   private LinkedList<Long> stack;

   //main method
   public static void main (String[] argum) {
      long i = interpret("2 15 -");
   }

   //Constructor of LongStack class
   LongStack()
   {
        stack = new LinkedList<Long>();
   }

   @Override
   //override clone method
   public Object clone() throws CloneNotSupportedException {
      LongStack tmp = new LongStack ();
      tmp.stack = (LinkedList<Long>) stack.clone();
      return tmp;
   }

   //check if stack list is empty or not
   public boolean stEmpty() {
      return (stack.isEmpty()) ? true : false;
   }

   //add elemnt to stack list
   public void push (long a) {
      stack.push(a);
   }

   //take top element from stack list
   public long pop() {
      if (!stEmpty()) {
         return stack.pop();
      } else {
         throw new RuntimeException("Stack is empty");
      }
   }

   //possible operatsions with stack elements
   public void op (String s) {
      if (s.equals("+")) {
         //take two elements from stack, add them and push result back to stack
         this.stack.push(this.stack.pop() + this.stack.pop());
      } else if (s.equals("-")) {
         long r1 = this.pop();
         long r2 = this.pop();
         //take two elements from stack, substract them and push result back to stack
         this.stack.push(r2 - r1);
      } else if (s.equals("*")) {
         //take two elements from stack, multiply them and push result back to stack
         this.stack.push(this.stack.pop() * this.stack.pop());
      } else if (s.equals("/")) {
         long r1 = this.pop();
         long r2 = this.pop();
         //take two elements from stack, divide them and push result back to stack
         this.stack.push(r2 / r1);
      } else if(s.equals(" ") | s.equals("\t")) {
         //if no operatsion sign then just return
         return;
      } else {
         throw new RuntimeException("Invalid operation");
      }
   }

   //check the top elemet but not take it out
   public long tos() {
      if (!stEmpty()) {
         return stack.peek();
      } else {
         throw new RuntimeException("Stack is Empty");
      }
   }

   @Override
   public boolean equals (Object o) {
      return this.stack.equals(((LongStack)o).stack);

   }

   @Override
   //convert all the elements in the stack to string
   public String toString() {
      StringBuffer ret = new StringBuffer();
      Iterator<Long> i = this.stack.descendingIterator();
      while (i.hasNext()) {
         ret.append(i.next());
         ret.append(" ");
      }
      return ret.toString();
   }

   //review stack and calculated results
   public static long interpret (String pol) {
      LongStack ls = new LongStack();
      if(pol.length()== 0){
         throw new RuntimeException("Input string is empty");
      }
      for(int i = 0; i < pol.length(); i++) {
         char c = pol.charAt(i);
         char nc = ' ';
         if (i+1 < pol.length()) {
            nc = pol.charAt(i + 1);
         }
         if ((c == '-' & nc >= '0' & nc <= '9')| (c >= '0' & c <= '9')) {
            StringBuilder buf = new StringBuilder();
            buf.append(c);
            i++;
            for (; i < pol.length(); i++) {
               c = pol.charAt(i);
               if (c >= '0' & c <= '9') {
                  buf.append(c);
               } else {
                  break;
               }
            }
            ls.push(Long.parseLong(buf.toString()));
         } else {
            ls.op("" + c);
         }
      }
      if (ls.stack.size() == 1) {
         return ls.pop();
      } else {
         throw new RuntimeException("Unbalanced polish notation");
      }
   }
}

